# Apigee Api Test
This repository is a POC to test Apigee API with Policy could work in a Postman/Newman API e2e automation.
And this repository also, include a a basic CI/CD pipeline configuration.

### Installation
To install this repository, you need to clone this project first in your local machine. After that, you need to do a `npm install` to install all devDependecies.

### Running the Test
To run the test without generating any report, you need to type `npm run e2e` in your terminal. And if you want to run the test with a report, the command is `npm run e2e:report`.

### Documentation
- [Apigee](https://cloud.google.com/apigee/docs/api-platform/get-started/eval-orgs)
- [Newman](https://github.com/postmanlabs/newman)
- [Newman Reporter](https://www.npmjs.com/package/newman-reporter-htmlextra)