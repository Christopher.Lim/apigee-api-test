const newman = require('newman');

newman.run({
    collection: 'src/test/collections/apigee.spec.json', // Collection URL from a public link or the Postman API can also be used
    reporters: ['htmlextra', 'json'],
    iterationCount: 1,
    reporter: {
        htmlextra: {
            logs: true,
        }
    }
});